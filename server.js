const http=require("http"),
      os=require("os");
const { normalize } = require("path");
 const nets=os.networkInterfaces();
 const results={};
 
for ( const name of Object.keys(nets)) {
    for ( const net of nets[name]) {
        const familyV4Value=typeof net.family=="string"?"IPv4":4;
        if (net.family===familyV4Value && ! net.internal){
            if (!results[name]) {
                results[name]=[];
            }
            results[name].push(net.address)
        }
    }
}  

console.log(results);

http.createServer((req,res)=>{
    const datetime=new Date(),
          load=os.loadavg(),
          doc=`
            <html>
                <head>
                    <meta charset="UTF-8">
                </head>
                <body>
                    <h1>Hallo Matthias und André...(A:42)</h1>
                    <h3>Zeit: ${datetime} </h3>
                    <h3>Auslastung: ${load[0]} %</h3>
                    <hr/>
                    <h3>IP-Adressen: ${nets} </h3>
                    <pre>${JSON.stringify(results)}</pre>
                </body>
            </html>
          `;
        res.setHeader("content-type","text/html");
        res.end(doc);
})
.listen(8080);
console.log("Server wartet...");