FROM node:10
COPY server.js /src/
USER node

# wird beim Starten des Containers ausgeführt
CMD ["node","/src/server.js"]

# docker build -t js-sample .

